import UIKit
import app

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    private let viewModel = SampleViewModel(platformProvider: IOSPlatformProvider())

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.observable.observe { value in
            self.label.text = value as? String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

package sample

import io.mockk.every
import io.mockk.mockk
import sample.platform_specific.PlatformProvider
import kotlin.test.Test
import kotlin.test.assertTrue

class SampleTests {

    private val provider: PlatformProvider = mockk()
    private lateinit var subject: SampleViewModel

    @Test
    fun `should add provided name to text`() {
        val expected = "Expected"
        every { provider.platformName } returns expected
        subject = SampleViewModel(provider)

        val actual  = subject.observable.getValue()

        assertTrue { actual?.contains(expected) == true }
    }

}
package sample.platform_specific

expect abstract class Observable<T>() {
    fun getValue(): T?
}

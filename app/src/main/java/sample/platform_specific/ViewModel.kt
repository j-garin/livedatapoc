package sample.platform_specific

import androidx.lifecycle.ViewModel as AndroidViewModel

actual typealias ViewModel = AndroidViewModel

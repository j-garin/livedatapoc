package sample.platform_specific

actual class MutableObservable<T> : Observable<T>() {

    actual fun newValue(value: T) {
        currentValue = value
        observer?.invoke(value)
    }
}

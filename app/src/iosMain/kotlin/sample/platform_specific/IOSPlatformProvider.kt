package sample.platform_specific

class IOSPlatformProvider : PlatformProvider {
    override val platformName: String = "iOS"
}